//Appending close button dynamically for existing list items
var li = document.getElementsByTagName("LI");
//console.log(li);
let i;
let liLen = li.length;
for(i = 0; i < liLen; i++){
  let span = document.createElement("SPAN");
  let spanText = document.createTextNode("\u00D7");
  span.appendChild(spanText);
  li[i].append(span);
}

//Delete Functionality
let delMovie = document.getElementsByTagName("SPAN");
let deleteLen = delMovie.length;
for (i = 0; i < deleteLen; i++) {
  delMovie[i].onclick = function() {
    var div = this.parentElement;
    //console.log(div);
    //div.style.display = "none";
    div.remove();
  };
}

//Execute add functionality on pressing enter key on keyboard
let input = document.getElementById("input");
input.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
      addMovie();
    }
});

//Add Functionality
  document.getElementById("addBtn").addEventListener("click", addMovie);
  function addMovie()
  {
    let input = document.getElementById("input").value; // Get the user entered value(Movie)
    let li = document.createElement("LI"); // Creating LI element
    //li.className("list-group-item"); -> Why is this not working
    li.setAttribute("class", "list-group-item");
    //li.setAttribute("id", "movieName");
    let liText = document.createTextNode(input); // Creating a text node with the user input
    li.appendChild(liText); // Appending the user input to the li
    let ul = document.getElementById("movieUl"); // Getting the UL 
    if(input == ""){
      alert("Movie name is required");
    }    
    /* else if(input.indexOf(liText > -1){
      alert(`testing`);
    } */
    else{
      ul.appendChild(li);// Appending Li to the UL
    }
    document.getElementById("input").value = ""; // Clearing the input value after appending it to ul.
    let span = document.createElement("SPAN"); // Creating a span element
    let spanText = document.createTextNode("\u00D7"); // Creating X for the span text 
    span.setAttribute("class", "delete");
    //span.className = "delete"; // Adding delete class to the created span element
    span.appendChild(spanText); // Appending span text to the span element
    li.appendChild(span); // Appending span to li element
    let delMovie = document.getElementsByClassName("delete"); // Getting the span element
    let i;
    let deleteLen = delMovie.length;
    for (i = 0; i < deleteLen; i++) { // looping through span elements
      delMovie[i].onclick = function() {
      var div = this.parentElement; // getting the parent element of span which os li
      div.remove();
      //div.style.display = "none"; // Hiding it on click
    };
  }
};

//Execute add functionality on pressing enter key on keyboard
let filter = document.getElementById("filter");
filter.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
    filterFun();
    }
});

//Filter Functionality
document.getElementById("filterBtn").addEventListener("click", filterFun);
function filterFun(){   
  let listItemValue = document.getElementsByClassName("list-group-item");
  let filterValue = document.getElementById("filter").value;
  if(filterValue == ""){
    alert("Enter something to filter");
  }
  let i;
  let listLen = listItemValue.length;
  for(i = 0; i <= listLen; i++){
    let movieName = listItemValue[i].innerText;
    console.log(movieName);
    if(movieName.indexOf(filterValue) > -1){
      listItemValue[i].style.display = "";
    }
    else{
      listItemValue[i].style.display = "none";
    }
  } 
}