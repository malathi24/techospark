var movieArr = [];
function addMovie() {
   var movieName = document.getElementById('input');
   if(movieName.value == ""){
     alert(`Enter Movie Name`);
   }
   else{
      movieArr.push(movieName.value);
      movieName.value = '';
      var movieyLi ="";
      let i;
      let movieArrLen = movieArr.length;
      for (i=0; i < movieArrLen; i++) {
        movieyLi+= `<li>${movieArr[i]}<span>X</span></li>`
      }
      var movieUl = document.getElementById('movieUl');
      movieUl.innerHTML = movieyLi;
  }
}
//Execute add functionality on pressing enter key on keyboard
let input = document.getElementById("input");
input.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
      addMovie();
    }
});